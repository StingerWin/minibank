<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'MoneyTransferController@create')->name('money_transfer.create');
Route::post('/money_transfer/store', 'MoneyTransferController@store')->name('money_transfer.store');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/cards', 'CardController');
    Route::get('/money_transfer', 'MoneyTransferController@index')->name('money_transfer.index')->middleware('can:admin-rights');
    Route::get('/users', 'HomeController@users')->name('users')->middleware('can:admin-rights');
});
