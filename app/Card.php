<?php

namespace App;

use App\Http\Controllers\CardController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Card extends Model
{

    protected $fillable = ['id_user', 'number_card', 'cvv', 'card_type', 'date_card', 'cart_at_moment'];

    public function accounts() {
        return $this->hasMany(Account::class, 'id_card', 'id');
    }

    public static function validatorCard($request)
    {
        $validator = Validator::make($request->all(), [
            'card_type' => [
                'required'
            ]
        ]);

        if (!array_key_exists($validator->validate()['card_type'], CardController::CARDS_TYPES)) {
            $validator->errors()->add('card_type', 'Such a card does not exist!');

        }

        if ($validator->errors()->messages()) {
            return redirect(route('cards.index'))
                ->withErrors($validator);
        }

        return $validator;
    }
}
