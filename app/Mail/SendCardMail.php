<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class SendCardMail extends Mailable
{
    use Queueable, SerializesModels;
    public $card;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($card)
    {
        $this->card = $card;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $card = $this->card;
        return $this->from(config('mail.email'), config('app.name'))->to(Auth::user()->email ?? config('mail.email'))->subject('New card - '.Crypt::decrypt($card->number_card))->view('emails.card-message');
    }
}
