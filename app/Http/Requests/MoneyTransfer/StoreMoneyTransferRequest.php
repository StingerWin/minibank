<?php

namespace App\Http\Requests\MoneyTransfer;

use Illuminate\Foundation\Http\FormRequest;

class StoreMoneyTransferRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number_card_recipient' => [
                'required',
                'numeric',
            ],
            'amount_transfer' => [
                'required',
                'numeric',
            ],
            'number_card_sender' => [
                'required',
                'numeric',
            ],
            'date_card' => [
                'required',
            ],
            'cvv_card' => [
                'required',
                'numeric',
            ],
        ];
    }
}
