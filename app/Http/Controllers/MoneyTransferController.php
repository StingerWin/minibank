<?php

namespace App\Http\Controllers;

use App\Account;
use App\Card;
use App\Http\Requests\MoneyTransfer\StoreMoneyTransferRequest;
use App\MoneyTransfer;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class MoneyTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $money_transfers = MoneyTransfer::with('card_sender', 'card_recipient')->orderByDesc('created_at')->paginate(5);

        return view('money_transfer.index', compact('money_transfers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('money_transfer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMoneyTransferRequest $request)
    {
        $validator = $request->validated();

        $valid_number_card_sender = false;
        $cards = Card::with('accounts')->get();

        $valid_error = [];
        foreach ($cards as $card) {
            if (Crypt::decrypt($card->number_card) == $validator['number_card_sender']) {
                $card_user_sender = $card;
                $valid_number_card_sender = true;
                if (Crypt::decrypt($card_user_sender->date_card) != $validator['date_card']) {
                    $valid_error['date_card'] = 'Such date is not correct!';
                }

                if (Crypt::decrypt($card_user_sender->cvv) != $validator['cvv_card']) {
                    $valid_error['cvv_card'] = 'Such CVV is not correct!';
                }

                $card_user_sender_money = (!$card_user_sender->accounts->isEmpty()) ? $card_user_sender->accounts->last()->remainder_after_surgery : 0;
                if ($card_user_sender_money < $validator['amount_transfer']) {
                    $valid_error['amount_transfer'] = 'Insufficient funds on the card!';
                }
            }
        }

        if (!$valid_number_card_sender) {
            $valid_error['number_card_sender'] = 'Such a card does not exist!';
        }

        $valid_number_card_recipient = false;
        foreach ($cards as $card) {
            if (Crypt::decrypt($card->number_card) == $validator['number_card_recipient']) {
                $valid_number_card_recipient = true;
                $card_user_recipient = $card;
            }
        }

        if (!$valid_number_card_recipient) {
            $valid_error['number_card_recipient'] = 'Such a card does not exist!';
        }

        if (!empty($valid_error)) {
            return redirect()->back()->withErrors($valid_error)->withInput();
        }


        DB::transaction(function () use ($card_user_sender_money, $validator, $card_user_recipient, $card_user_sender) {
            MoneyTransfer::create([
                'id_card_sender' => $card_user_sender->id,
                'id_card_recipient' => $card_user_recipient->id,
                'amount_transfer' => $validator['amount_transfer'],
            ]);


            Account::create([
                'id_card' => $card_user_sender->id,
                'amount_charge' => '-'.$validator['amount_transfer'],
                'remainder_after_surgery' => $card_user_sender_money - $validator['amount_transfer'],
            ]);

            $card_user_recipient_money = (!$card_user_recipient->accounts->isEmpty()) ? $card_user_recipient->accounts->last()->remainder_after_surgery : 0;
            Account::create([
                'id_card' => $card_user_recipient->id,
                'amount_charge' => $validator['amount_transfer'],
                'remainder_after_surgery' => $card_user_recipient_money + $validator['amount_transfer'],
            ]);
        });

        return redirect()->route('money_transfer.create')->with('message_transfer', 'Деньги перечислены!');
    }
}
