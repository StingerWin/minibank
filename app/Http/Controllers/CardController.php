<?php

namespace App\Http\Controllers;

use App\Card;
use App\Events\CardEvent;
use App\Jobs\SendMessageCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class CardController extends Controller
{
    const CARDS_TYPES = [
        '1' => 'Visa',
        '2' => 'Mastercard',
    ];

    public function index()
    {
        $cards = Auth::user()->cards;
        $cart_at_moment = $cards->where('cart_at_moment', true)->first()->load('accounts');
        $cards = $cards->where('cart_at_moment', false)->load('accounts');

        $cards_types = self::CARDS_TYPES;


        return view('cards.index', compact('cards', 'cards_types', 'cart_at_moment'));
    }

    public function store(Request $request)
    {
        $validator = Card::validatorCard($request);

        $validation = $validator->validate();

        $id_user = Auth::user()->id;
        $carts_at_moment = Card::where('id_user', $id_user)->where('cart_at_moment', true)->get();
        foreach ($carts_at_moment as $cart_at_moment) {
            $cart_at_moment->update(['cart_at_moment' => 0]);
        }

        $validation['id_user'] = $id_user;
        $validation['number_card'] = Crypt::encrypt(rand(1000000000000000, 9999999999999999));
        $validation['date_card'] = Crypt::encrypt(now()->format('m/Y'));
        $validation['cvv'] = Crypt::encrypt(rand(100, 999));
        $validation['cart_at_moment'] = 1;

        $card = Card::create($validation);

        event(new CardEvent($card, 50));

        $this->dispatch(new SendMessageCard($card));

        return redirect()->route('cards.index');
    }
}
