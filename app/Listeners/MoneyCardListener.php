<?php

namespace App\Listeners;

use App\Account;
use App\Events\CardEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MoneyCardListener implements ShouldQueue
{

    /**
     * @param $event
     */
    public function handle(CardEvent $event)
    {
        if (isset($event->card) and !empty($event->amount_money)) {
            $account_last = $event->card->load('accounts')->accounts->last();
            Account::create([
                'id_card' => $event->card->id,
                'amount_charge' => $event->amount_money,
                'remainder_after_surgery' => $account_last ? $account_last->remainder_after_surgery : 0 + $event->amount_money,
            ]);
        }

    }
}
