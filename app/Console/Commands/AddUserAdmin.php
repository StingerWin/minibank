<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class AddUserAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add_admin {email} {password} {pass}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user with admin right';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            'name' => 'Админ',
            'email' => $this->argument('email'),
            'password' => bcrypt($this->argument('pass')),
            'rights' => User::RIGHTS_ADMIN,
        ];
        if (User::create($data)) {
            echo 'Add user '.$this->argument('email');
        }
    }
}
