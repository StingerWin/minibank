<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyTransfer extends Model
{
    protected $fillable = ['id_card_sender', 'id_card_recipient', 'amount_transfer'];

    public function card_sender()
    {
        return $this->belongsTo(Card::class, 'id_card_sender', 'id');
    }

    public function card_recipient()
    {
        return $this->belongsTo(Card::class, 'id_card_recipient', 'id');
    }
}
