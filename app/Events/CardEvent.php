<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CardEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $card;
    public $amount_money;

    /**
     * CardEvent constructor.
     * @param $card
     * @param $amount_money
     */
    public function __construct($card, $amount_money)
    {
        $this->card = $card;
        $this->amount_money = $amount_money;
    }
}
