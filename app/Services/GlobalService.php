<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class GlobalService
{

    public static function saveFile($data, $key, $path)
    {
        if (array_key_exists($key, $data)) {
            $fileName = $data[$key]->storeAs(
                'public/'.$path, time() . '-' . $data[$key]->getClientOriginalName()
            );
            return '/storage/' . str_replace('public/', '', $fileName);
        }
        return null;
    }
}
