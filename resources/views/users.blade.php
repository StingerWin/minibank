@extends('layouts.app')

@section('title', 'Пользователи')

@section('styles')
@endsection

@section('content')
    <br>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header">
                        <h3 class="card-title" style="display: contents">Список пользователей
                            ({{count($users)}})</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table_scrollx">
                        <table id="cards_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Имя</th>
                                <th>Email</th>
                                <th>Дата регистрации</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->created_at->format('d.m.Y H:i')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{$users->links()}}
                    </div>

                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('scripts')

@endsection
