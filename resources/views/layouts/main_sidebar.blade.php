<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('money_transfer.create')}}" class="brand-link">
        <img src="{{URL::asset('assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light"><b>CRM</b> ITEye</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            @auth
                @if(Auth::user()->image)
                    <div class="image">
                        <img src="{{URL::asset(Auth::user()->image)}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                @endif
                <div class="info">
                    <a href="#" class="d-block">{{ Auth::user()->name}}</a>
                </div>
            @endauth
        </div>

        <!-- Sidebar MenuItem -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent text-sm" data-widget="treeview"
                role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('money_transfer.create')}}"
                       class="nav-link {{Route::is('money_transfer.create') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-clipboard-list"></i>
                        <p>Перевод денег</p>
                    </a>
                </li>
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item">
                            <a href="{{route('cards.index')}}"
                               class="nav-link {{Route::is('cards.*') ? 'active' : ''}}">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Банковские карткы</p>
                            </a>
                        </li>
                        @if (Auth::user()->isAdmin())
                            <li class="nav-item">
                                <a href="{{route('money_transfer.index')}}"
                                   class="nav-link {{Route::is('money_transfer.index') ? 'active' : ''}}">
                                    <i class="nav-icon fas fa-clipboard-list"></i>
                                    <p>Банковские переводы</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('users')}}"
                                   class="nav-link {{Route::is('users') ? 'active' : ''}}">
                                    <i class="nav-icon fas fa-clipboard-list"></i>
                                    <p>Пользователи</p>
                                </a>
                            </li>
                        @endif
                    @endauth
                @endif
                {{--                @if($globalRights['isAdmin'])--}}

                {{--                @endif--}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
