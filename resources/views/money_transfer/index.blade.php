@extends('layouts.app')

@section('title', 'Банковские переводы')

@section('styles')
@endsection

@section('content')
    <br>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header">
                        <h3 class="card-title" style="display: contents">Список переводов
                            ({{count($money_transfers)}})</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table_scrollx">
                        <table id="cards_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Номер картки отправителя</th>
                                <th>Номер картки получателя</th>
                                <th>Сумма</th>
                                <th>Дата</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($money_transfers as $money_transfer)
                                <tr>
                                    <td>{{$money_transfer->id}}</td>
                                    <td>{{Crypt::decrypt($money_transfer->card_sender->number_card)}}</td>
                                    <td>{{Crypt::decrypt($money_transfer->card_recipient->number_card)}}</td>
                                    <td>{{$money_transfer->amount_transfer}}</td>
                                    <td>{{$money_transfer->created_at->format('d.m.Y H:i')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{$money_transfers->links()}}
                    </div>

                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('scripts')

@endsection
