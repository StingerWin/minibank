@extends('layouts.app')

@section('title', 'Банковские карты')

@section('styles')
@endsection

@section('content')
    <br>
    <section class="content">
        <div class="row">
            <div class="col-12">
                @if (session('message_transfer'))
                    <div class="alert alert-success">
                        {{ session('message_transfer') }}
                    </div>
                @endif
            </div>
            <div class="col-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    {!! Form::open([
                        'method' => 'STORE',
                        'url' => route('money_transfer.store'),
                        'role' => 'form',
                        'files' => true,
                    ]) !!}
                    <div class="card-header">
                        <h2 class="card-title col-12">
                            Перевод денег
                        </h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('number_card_recipient') ? ' has-error' : ''}}">
                                    <label for="number_card_recipient"><small>Номер карты получателя</small></label>
                                    {!! Form::text('number_card_recipient', null, ['class' => $errors->has('number_card_recipient') ? 'form-control is-invalid' : 'form-control', 'id' => 'number_card_recipient', 'placeholder' => 'Введите номер карты получателя', 'required' => 'required', 'pattern' => '\d*', 'minlength' => '16', 'maxlength' => '16']) !!}
                                    {!! $errors->first('number_card_recipient', '<p class="help-block" style="color:#dc3545">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('amount_transfer') ? ' has-error' : ''}}">
                                    <label for="amount_transfer"><small>Сумма перевода</small></label>
                                    {!! Form::text('amount_transfer', null, ['class' => $errors->has('amount_transfer') ? 'form-control is-invalid' : 'form-control', 'id' => 'amount_transfer', 'placeholder' => 'Введите сумму перевода', 'required' => 'required', 'pattern' => '\d*']) !!}
                                    {!! $errors->first('amount_transfer', '<p class="help-block" style="color:#dc3545">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('number_card_sender') ? ' has-error' : ''}}">
                                    <label for="number_card_sender"><small>Номер карты отправителя</small></label>
                                    {!! Form::text('number_card_sender', null, ['class' => $errors->has('number_card_sender') ? 'form-control is-invalid' : 'form-control', 'id' => 'number_card_sender', 'placeholder' => 'Введите номер карты отправителя', 'required' => 'required', 'pattern' => '\d*', 'minlength' => '16', 'maxlength' => '16']) !!}
                                    {!! $errors->first('number_card_sender', '<p class="help-block" style="color:#dc3545">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('date_card') ? ' has-error' : ''}}">
                                    <label for="date_card"><small>Дата (пример - 04/2020)</small></label>
                                    {!! Form::text('date_card', null, ['class' => $errors->has('date_card') ? 'form-control is-invalid' : 'form-control', 'id' => 'date_card', 'placeholder' => 'Введите дату карты', 'required' => 'required', 'pattern' => '[0-9]{2}\/[0-9]{4}']) !!}
                                    {!! $errors->first('date_card', '<p class="help-block" style="color:#dc3545">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('cvv_card') ? ' has-error' : ''}}">
                                    <label for="cvv_card"><small>CVV</small></label>
                                    {!! Form::text('cvv_card', null, ['class' => $errors->has('cvv_card') ? 'form-control is-invalid' : 'form-control', 'id' => 'cvv_card', 'placeholder' => 'Введите cvv карты', 'required' => 'required', 'pattern' => '\d*', 'minlength' => '3', 'maxlength' => '3']) !!}
                                    {!! $errors->first('cvv_card', '<p class="help-block" style="color:#dc3545">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Вы уверены?')">Перевести
                        </button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('scripts')

@endsection
