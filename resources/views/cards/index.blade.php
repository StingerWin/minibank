@extends('layouts.app')

@section('title', 'Банковские карты')

@section('styles')
@endsection

@section('content')
    <br>
    <section class="content">
        <div class="row">
            @isset($cart_at_moment)
                <div class="col-12">
                    <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header">
                            <h3 class="card-title">Настоящая карта</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table_scrollx">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Номер картки</th>
                                    <th>Тип</th>
                                    <th>Баланс</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$cart_at_moment->id}}</td>
                                    <td>{{Crypt::decrypt($cart_at_moment->number_card)}}</td>
                                    <td>{{$cards_types[$cart_at_moment->card_type]}}</td>
                                    <td>{{$cart_at_moment->accounts->last() ? $cart_at_moment->accounts->last()->remainder_after_surgery : ''}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            @endisset
            <div class="col-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header">
                        <h3 class="card-title" style="display: contents">Список карток
                            ({{count($cards)}})</h3>
                        <button class="btn btn-success waves-effect"
                                style="float: right" data-toggle="modal" data-target="#modal-cards">+ Создать новую
                        </button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table_scrollx">
                        <table id="cards_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Номер картки</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cards as $card)
                                <tr>
                                    <td>{{$card->id}}</td>
                                    <td>{{Crypt::decrypt($card->number_card)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Номер картки</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>

    <div class="modal fade" id="modal-cards">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                @include('cards.create')
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{URL::asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{URL::asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#cards_table").DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                // "order": [[1, "asc"]]
            });
        });
    </script>
@endsection
