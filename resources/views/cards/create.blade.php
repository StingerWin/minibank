<div class="modal-header">
    <h4 class="modal-title">Новый оборудование</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
{!! Form::open([
    'method' => 'STORE',
    'url' => route('cards.store'),
    'role' => 'form',
    'files' => true,
    ]) !!}
<div class="modal-body">
    @if ($errors->any())
        @include('components.validation_errors')
    @endif
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="form-group {{ $errors->has('card_type') ? ' has-error' : ''}}">
                <label for="card_type">Тип карты</label>
                {!! Form::select('card_type', $cards_types,
                null, ['class' => $errors->has('card_type') ? 'form-control select2 is-invalid' : 'form-control select2', 'id' => 'card_type',]) !!}
                {!! $errors->first('card_type', '<p class="help-block" style="color:#dc3545">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="submit" class="btn btn-primary" style="float: right">Создать</button>
</div>

{!! Form::close() !!}

