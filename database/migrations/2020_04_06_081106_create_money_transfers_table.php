<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneyTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_transfers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_card_sender')->unsigned();
            $table->bigInteger('id_card_recipient')->unsigned();
            $table->bigInteger('amount_transfer');

            $table->foreign('id_card_sender')->references('id')->on('cards')->onDelete('cascade');
            $table->foreign('id_card_recipient')->references('id')->on('cards')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_transfers');
    }
}
